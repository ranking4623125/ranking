def relation(std1, std2) -> str:
    marks1 = std1.split(' ')[1:]
    marks2 = std2.split(' ')[1:]
    if all(x > y for x, y in zip(marks1, marks2)):
        return '>'
    elif all(x < y for x, y in zip(marks1, marks2)):
        return '<'    
    else:
        return '#'
        
from itertools import combinations    
def pass_marks():
    students_list = []
    for line in ["A 12 14 16","B 5 6 7","C 17 20 23","D 2 40 12",
    "E 3 41 13", "F 7 8 9",
    "G 4 5 6"]:
        students_list.append(line)
    relations = []
    combos = combinations(students_list, 2)
    for combo in combos:
        relations.append(relation(combo[0],combo[1]))
    return relations
       
    
print(relation("a 12 13 15 17 18", "b 13 17 18 19 21"))
