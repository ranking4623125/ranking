

def comparing_students(student1, student2) -> bool:
    name1, marks1 = student1
    name2, marks2 = student2
    if all(m1 > m2 for m1, m2 in zip(marks1, marks2)):
        return True
    else:
        return False
def ranking_students(students):
    ranked = []
    unranked = []
    
    for _, student in enumerate(students):
        placed = False
        for j in range(len(ranked)):
            comparison = comparing_students(student, ranked[j])
            if comparison == True :
                ranked.insert(j, student)
                placed = True
                break
            elif comparison != True:
                continue
        if not placed:
            ranked.append(student)
    
    for _ in range(len(ranked) - 1):
        for j in range(_ + 1, len(ranked)):
            if comparing_students(ranked[_], ranked[j]) == 0:
                unranked.append(ranked.pop(j))
                unranked.append(ranked.pop(_))
                break
    
    return ranked, unranked


students = [
    ("A", [12, 14, 16]),
    ("B", [5, 6, 7]),
    ("C", [17, 20, 23]),
    ("D", [2, 40, 12]),
    ("E", [3, 41, 13]),
    ("F", [7, 8, 9]),
    ("G", [4, 5, 6])
]
print(ranking_students(students))